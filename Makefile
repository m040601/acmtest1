all: dir pdf html

# Build a number of formats into the `outy/` directory. These will get published via Github pages.
dir:
	mkdir -p outy

pdf:
	pandoc --smart --filter pandoc-citeproc --variable documentclass=templates/sig-alternate --csl=templates/acm-sig-proceedings.csl --highlight-style=haddock -f markdown -t latex -o outy/paper.pdf paper.md
html:
	pandoc --smart --filter pandoc-citeproc --standalone --css=pandoc.css --toc --highlight-style=haddock -f markdown -t html -o outy/index.html paper.md
	cp templates/pandoc.css outy/pandoc.css


